package com.melita.interview.interviewcandidate16161563.resources.errors;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import org.springframework.http.HttpStatus;

import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
@Builder
@Getter
public class ErrorResponse {
    private final HttpStatus status;
    @Singular
    @JsonInclude(NON_EMPTY)
    private final List<Error> errors;
}
