package com.melita.interview.interviewcandidate16161563.resources.errors;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.melita.interview.interviewcandidate16161563.resources.errors.ErrorResponse;
import com.melita.interview.interviewcandidate16161563.resources.errors.ErrorResponse.ErrorResponseBuilder;
import com.melita.interview.interviewcandidate16161563.resources.errors.ValidationError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
@ControllerAdvice(annotations = RestController.class)
@Slf4j
public class ErrorMapper {

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handleValidationExceptions(final MethodArgumentNotValidException exception) {
        final ErrorResponseBuilder errorBuilder = ErrorResponse.builder().status(BAD_REQUEST);

        exception.getBindingResult().getAllErrors().forEach(error ->
                errorBuilder.error(new ValidationError(((FieldError) error).getField(), error.getDefaultMessage())));

        return errorBuilder.build();
    }

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ErrorResponse handleMessageNotReadableException(final HttpMessageNotReadableException exception) {
        final ErrorResponseBuilder errorBuilder = ErrorResponse.builder().status(BAD_REQUEST);

        final Throwable cause = exception.getCause();

        if (cause instanceof InvalidFormatException) {
            final InvalidFormatException invalidFormatException = (InvalidFormatException) cause;
            invalidFormatException.getPath().forEach(reference ->
                    errorBuilder.error(new ValidationError(reference.getFieldName(), String.format("[%s] is not a valid format", invalidFormatException.getValue()))));
        }

        return errorBuilder.build();
    }

    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler(RuntimeException.class)
    public ErrorResponse handleMessageNotReadableException(final RuntimeException runtimeException) {
        log.error("Internal Server Error", runtimeException);
        return ErrorResponse.builder()
                .status(INTERNAL_SERVER_ERROR)
                .build();
    }
}
