package com.melita.interview.interviewcandidate16161563.resources.customers.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
@NoArgsConstructor
@Data
public class CustomerDetails {

    @NotBlank(message = "Name is required")
    @Size(max = 50, message = "Name maximum length is 50 characters")
    private String name;

    @NotBlank(message = "Surname is required")
    @Size(max = 50, message = "Surname maximum length is 50 characters")
    private String surname;

    @NotNull(message = "Customer Type is required")
    @Valid
    private CustomerType type;

    @NotNull(message = "Customer Status is required")
    @Valid
    private CustomerStatus status;
}
