package com.melita.interview.interviewcandidate16161563.resources.customers;

import com.melita.interview.interviewcandidate16161563.resources.customers.models.CustomerDetails;
import com.melita.interview.interviewcandidate16161563.processes.CreateCustomerDetailsProcess;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.CREATED;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
@RestController
@RequestMapping(CustomerResource.RESOURCE)
@RequiredArgsConstructor
public class CustomerResource {

    public static final String RESOURCE = "customers";

    private final CreateCustomerDetailsProcess createCustomerDetailsProcess;

    @PostMapping
    @ResponseStatus(CREATED)
    public void createCustomerInfo(@Valid @RequestBody final CustomerDetails customerDetails) {
        createCustomerDetailsProcess.create(customerDetails);
    }

}
