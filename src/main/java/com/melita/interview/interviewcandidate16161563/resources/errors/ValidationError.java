package com.melita.interview.interviewcandidate16161563.resources.errors;

import lombok.Data;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
@Data
public class ValidationError implements Error {
    private final String field;
    private final String message;
}
