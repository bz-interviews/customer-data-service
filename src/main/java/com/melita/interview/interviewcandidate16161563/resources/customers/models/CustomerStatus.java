package com.melita.interview.interviewcandidate16161563.resources.customers.models;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
public enum CustomerStatus {
    ACTIVE,
    SUSPENDED,
    CEASED
}
