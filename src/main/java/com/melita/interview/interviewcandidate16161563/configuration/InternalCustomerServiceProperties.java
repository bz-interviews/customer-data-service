package com.melita.interview.interviewcandidate16161563.configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com">Brian Zammit</a>
 */
@Component
@Getter
public class InternalCustomerServiceProperties {

    @Value("${internalCustomerService.candidateId}")
    private String candidateId;

    @Value("${internalCustomerService.baseUrl}")
    private String baseUrl;
}
