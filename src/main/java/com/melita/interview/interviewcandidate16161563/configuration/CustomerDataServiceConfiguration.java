package com.melita.interview.interviewcandidate16161563.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
@Configuration
@EnableSwagger2
public class CustomerDataServiceConfiguration {

    public static final String INTERNAL_CUSTOMER_SERVICE_REST_TEMPLATE = "INTERNAL_CUSTOMER_SERVICE_REST_TEMPLATE";
    private final String CANDIDATE_ID_HEADER = "candidate-id";

    @Bean
    @Qualifier(INTERNAL_CUSTOMER_SERVICE_REST_TEMPLATE)
    public RestTemplate restTemplate(final InternalCustomerServiceProperties internalCustomerServiceProperties) {
        return new RestTemplateBuilder()
                .defaultHeader(CANDIDATE_ID_HEADER, internalCustomerServiceProperties.getCandidateId())
                .defaultHeader("content-type", "application/json")
                .build();
    }
}
