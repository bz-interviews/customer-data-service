package com.melita.interview.interviewcandidate16161563.processes.mappers;

import com.melita.interview.interviewcandidate16161563.connectors.customers.requests.catalog.CustomerTypeRequest;
import com.melita.interview.interviewcandidate16161563.resources.customers.models.CustomerType;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Map;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
@Component
public class CustomerTypeMapper extends EnumMapper<CustomerType, CustomerTypeRequest> {

    private static final Map<CustomerType, CustomerTypeRequest> REQUEST_STATUS_MAPPER;

    static {
        REQUEST_STATUS_MAPPER = new EnumMap<>(CustomerType.class);
        REQUEST_STATUS_MAPPER.put(CustomerType.REGULAR, CustomerTypeRequest.REGULAR);
        REQUEST_STATUS_MAPPER.put(CustomerType.BUSINESS, CustomerTypeRequest.BUSINESS);
    }

    @Override
    protected Map<CustomerType, CustomerTypeRequest> getMappings() {
        return REQUEST_STATUS_MAPPER;
    }
}
