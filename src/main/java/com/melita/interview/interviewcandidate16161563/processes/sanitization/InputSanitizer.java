package com.melita.interview.interviewcandidate16161563.processes.sanitization;

import org.owasp.esapi.ESAPI;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
@Component
public class InputSanitizer {

    public String sanitize(final String input) {
        final String htmlEncoded = ESAPI.encoder().encodeForHTML(input);
        return ESAPI.encoder().encodeForJavaScript(htmlEncoded);
    }
}
