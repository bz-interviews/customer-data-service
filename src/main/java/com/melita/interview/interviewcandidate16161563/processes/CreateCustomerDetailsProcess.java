package com.melita.interview.interviewcandidate16161563.processes;

import com.melita.interview.interviewcandidate16161563.connectors.customers.InternalCustomerServiceStub;
import com.melita.interview.interviewcandidate16161563.connectors.customers.requests.CreateCustomerRequest;
import com.melita.interview.interviewcandidate16161563.resources.customers.models.CustomerDetails;
import com.melita.interview.interviewcandidate16161563.processes.mappers.CustomerStatusMapper;
import com.melita.interview.interviewcandidate16161563.processes.mappers.CustomerTypeMapper;
import com.melita.interview.interviewcandidate16161563.processes.sanitization.InputSanitizer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
@Component
@RequiredArgsConstructor
public class CreateCustomerDetailsProcess {

    private final InputSanitizer inputSanitizer;
    private final CustomerTypeMapper customerTypeMapper;
    private final CustomerStatusMapper customerStatusMapper;
    private final InternalCustomerServiceStub internalCustomerServiceStub;

    public void create(final CustomerDetails customerDetails) {
        internalCustomerServiceStub.createCustomer(CreateCustomerRequest.builder()
                .name(inputSanitizer.sanitize(customerDetails.getName()))
                .surname(inputSanitizer.sanitize(customerDetails.getSurname()))
                .type(customerTypeMapper.map(customerDetails.getType()))
                .status(customerStatusMapper.map(customerDetails.getStatus()))
                .build());
    }
}
