package com.melita.interview.interviewcandidate16161563.processes.mappers;

import com.melita.interview.interviewcandidate16161563.connectors.customers.requests.catalog.CustomerStatusRequest;
import com.melita.interview.interviewcandidate16161563.resources.customers.models.CustomerStatus;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Map;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
@Component
public class CustomerStatusMapper extends EnumMapper<CustomerStatus, CustomerStatusRequest> {

    private static final Map<CustomerStatus, CustomerStatusRequest> REQUEST_STATUS_MAPPER;

    static {
        REQUEST_STATUS_MAPPER = new EnumMap<>(CustomerStatus.class);
        REQUEST_STATUS_MAPPER.put(CustomerStatus.ACTIVE, CustomerStatusRequest.ACTIVE);
        REQUEST_STATUS_MAPPER.put(CustomerStatus.SUSPENDED, CustomerStatusRequest.SUSPENDED);
        REQUEST_STATUS_MAPPER.put(CustomerStatus.CEASED, CustomerStatusRequest.CEASED);
    }

    @Override
    protected Map<CustomerStatus, CustomerStatusRequest> getMappings() {
        return REQUEST_STATUS_MAPPER;
    }
}
