package com.melita.interview.interviewcandidate16161563.processes.mappers;

import java.util.Map;
import java.util.Optional;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
public abstract class EnumMapper<S extends Enum<S>, D extends Enum<D>> {

    protected abstract Map<S, D> getMappings();

    public D map(final S sourceValie) {
        final Optional<D> mappedValue = Optional.ofNullable(getMappings().get(sourceValie));
        return mappedValue.orElseThrow(() -> new IllegalStateException(String.format("Unable to map enum [%s]", sourceValie)));
    }
}
