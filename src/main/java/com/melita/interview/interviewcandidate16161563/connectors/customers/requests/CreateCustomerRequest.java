package com.melita.interview.interviewcandidate16161563.connectors.customers.requests;


import com.melita.interview.interviewcandidate16161563.connectors.customers.requests.catalog.CustomerStatusRequest;
import com.melita.interview.interviewcandidate16161563.connectors.customers.requests.catalog.CustomerTypeRequest;
import lombok.Builder;
import lombok.Value;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
@Value
@Builder
public class CreateCustomerRequest {
    private final String name;
    private final String surname;
    private final CustomerTypeRequest type;
    private final CustomerStatusRequest status;
}
