package com.melita.interview.interviewcandidate16161563.connectors.customers;

import com.melita.interview.interviewcandidate16161563.configuration.InternalCustomerServiceProperties;
import com.melita.interview.interviewcandidate16161563.connectors.customers.requests.CreateCustomerRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static com.melita.interview.interviewcandidate16161563.configuration.CustomerDataServiceConfiguration.INTERNAL_CUSTOMER_SERVICE_REST_TEMPLATE;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
@Component
public class InternalCustomerServiceStubImpl implements InternalCustomerServiceStub {

    private static final String RESOURCE = "customers";

    private final RestTemplate restTemplate;
    private final String url;

    @Autowired
    public InternalCustomerServiceStubImpl(@Qualifier(INTERNAL_CUSTOMER_SERVICE_REST_TEMPLATE) final RestTemplate restTemplate,
                                           final InternalCustomerServiceProperties internalCustomerServiceProperties) {
        this.restTemplate = restTemplate;
        this.url = internalCustomerServiceProperties.getBaseUrl() + RESOURCE;
    }

    @Override
    public void createCustomer(final CreateCustomerRequest request) {
        restTemplate.put(url, request);
    }
}
