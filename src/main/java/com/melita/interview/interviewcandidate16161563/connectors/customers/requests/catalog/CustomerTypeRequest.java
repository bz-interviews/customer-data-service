package com.melita.interview.interviewcandidate16161563.connectors.customers.requests.catalog;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com>Brian Zammit</a>
 */
public enum CustomerTypeRequest {
    REGULAR,
    BUSINESS
}
