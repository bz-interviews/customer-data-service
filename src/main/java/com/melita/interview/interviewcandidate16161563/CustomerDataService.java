package com.melita.interview.interviewcandidate16161563;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerDataService {

    public static void main(String[] args) {
        SpringApplication.run(CustomerDataService.class, args);
    }

}
