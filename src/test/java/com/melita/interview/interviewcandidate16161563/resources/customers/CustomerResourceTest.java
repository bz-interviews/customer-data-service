package com.melita.interview.interviewcandidate16161563.resources.customers;


import com.melita.interview.interviewcandidate16161563.resources.customers.models.CustomerDetails;
import com.melita.interview.interviewcandidate16161563.processes.CreateCustomerDetailsProcess;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static com.melita.interview.interviewcandidate16161563.resources.customers.models.CustomerStatus.ACTIVE;
import static com.melita.interview.interviewcandidate16161563.resources.customers.models.CustomerType.BUSINESS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
class CustomerResourceTest {

    private static final String CUSTOMERS = "/customers";

    private static final String NAME = "Joe";
    private static final String SURNAME = "Borg";
    private static final String LONG_51_CHARS_STRING = "abcdefghijabcdefghijabcdefghijabcdefghijabcdefghija";

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @MockBean
    private CreateCustomerDetailsProcess createCustomerDetailsProcess;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void createCustomer_success_created() throws Exception {
        postCustomerDetails(new CustomerDetailsJsonBuilder()
                .setName(NAME)
                .setSurname(SURNAME)
                .setType(BUSINESS.name())
                .setStatus(ACTIVE.name()))
                .andExpect(status().isCreated());

        final CustomerDetails submittedCustomerDetails = new CustomerDetails();
        submittedCustomerDetails.setName(NAME);
        submittedCustomerDetails.setSurname(SURNAME);
        submittedCustomerDetails.setType(BUSINESS);
        submittedCustomerDetails.setStatus(ACTIVE);

        verify(createCustomerDetailsProcess).create(eq(submittedCustomerDetails));
    }

    @Test
    void createCustomer_blankName_badRequest() throws Exception {
        postCustomerDetails(new CustomerDetailsJsonBuilder()
                .setName("  ")
                .setSurname(SURNAME)
                .setType(BUSINESS.name())
                .setStatus(ACTIVE.name()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(BAD_REQUEST.name()))
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].field").value("name"))
                .andExpect(jsonPath("$.errors[0].message").value("Name is required"));
    }

    @Test
    void createCustomer_nameNull_badRequest() throws Exception {
        postCustomerDetails(new CustomerDetailsJsonBuilder()
                .setSurname(SURNAME)
                .setType(BUSINESS.name())
                .setStatus(ACTIVE.name()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(BAD_REQUEST.name()))
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].field").value("name"))
                .andExpect(jsonPath("$.errors[0].message").value("Name is required"));
    }

    @Test
    void createCustomer_nameTooLong_badRequest() throws Exception {
        postCustomerDetails(new CustomerDetailsJsonBuilder()
                .setName(LONG_51_CHARS_STRING)
                .setSurname(SURNAME)
                .setType(BUSINESS.name())
                .setStatus(ACTIVE.name()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(BAD_REQUEST.name()))
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].field").value("name"))
                .andExpect(jsonPath("$.errors[0].message").value("Name maximum length is 50 characters"));
    }

    @Test
    void createCustomer_blankSurname_badRequest() throws Exception {
        postCustomerDetails(new CustomerDetailsJsonBuilder()
                .setName(NAME)
                .setSurname("  ")
                .setType(BUSINESS.name())
                .setStatus(ACTIVE.name()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(BAD_REQUEST.name()))
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].field").value("surname"))
                .andExpect(jsonPath("$.errors[0].message").value("Surname is required"));
    }

    @Test
    void createCustomer_surnameNull_badRequest() throws Exception {
        postCustomerDetails(new CustomerDetailsJsonBuilder()
                .setName(NAME)
                .setType(BUSINESS.name())
                .setStatus(ACTIVE.name()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(BAD_REQUEST.name()))
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].field").value("surname"))
                .andExpect(jsonPath("$.errors[0].message").value("Surname is required"));
    }

    @Test
    void createCustomer_surnameTooLong_badRequest() throws Exception {
        postCustomerDetails(new CustomerDetailsJsonBuilder()
                .setName(NAME)
                .setSurname(LONG_51_CHARS_STRING)
                .setType(BUSINESS.name())
                .setStatus(ACTIVE.name()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(BAD_REQUEST.name()))
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].field").value("surname"))
                .andExpect(jsonPath("$.errors[0].message").value("Surname maximum length is 50 characters"));
    }

    @Test
    void createCustomer_typeNull_badRequest() throws Exception {
        postCustomerDetails(new CustomerDetailsJsonBuilder()
                .setName(NAME)
                .setSurname(SURNAME)
                .setStatus(ACTIVE.name()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(BAD_REQUEST.name()))
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].field").value("type"))
                .andExpect(jsonPath("$.errors[0].message").value("Customer Type is required"));
    }

    @Test
    void createCustomer_typeInvalid_badRequest() throws Exception {
        postCustomerDetails(new CustomerDetailsJsonBuilder()
                .setName(NAME)
                .setSurname(SURNAME)
                .setType("INVALID_TYPE")
                .setStatus(ACTIVE.name()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(BAD_REQUEST.name()))
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].field").value("type"))
                .andExpect(jsonPath("$.errors[0].message").value("[INVALID_TYPE] is not a valid format"));
    }

    @Test
    void createCustomer_statusNull_badRequest() throws Exception {
        postCustomerDetails(new CustomerDetailsJsonBuilder()
                .setName(NAME)
                .setSurname(SURNAME)
                .setType(BUSINESS.name()))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(BAD_REQUEST.name()))
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].field").value("status"))
                .andExpect(jsonPath("$.errors[0].message").value("Customer Status is required"));
    }

    @Test
    void createCustomer_statusInvalid_badRequest() throws Exception {
        postCustomerDetails(new CustomerDetailsJsonBuilder()
                .setName(NAME)
                .setSurname(SURNAME)
                .setType(BUSINESS.name())
                .setStatus("INVALID_STATUS"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value(BAD_REQUEST.name()))
                .andExpect(jsonPath("$.errors").isNotEmpty())
                .andExpect(jsonPath("$.errors[0].field").value("status"))
                .andExpect(jsonPath("$.errors[0].message").value("[INVALID_STATUS] is not a valid format"));
    }

    @Test
    void createCustomer_runtimeExceptionOccurs_returnsInternalServerError() throws Exception {
        doThrow(new IllegalStateException()).when(createCustomerDetailsProcess).create(any(CustomerDetails.class));

        postCustomerDetails(new CustomerDetailsJsonBuilder()
                .setName(NAME)
                .setSurname(SURNAME)
                .setType(BUSINESS.name())
                .setStatus(ACTIVE.name()))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.status").value(INTERNAL_SERVER_ERROR.name()))
                .andExpect(jsonPath("$.errors").doesNotExist());
    }

    private ResultActions postCustomerDetails(final CustomerDetailsJsonBuilder customerDetailsJsonBuilder) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.post(CUSTOMERS)
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(customerDetailsJsonBuilder.toJsonString()));
    }

}