package com.melita.interview.interviewcandidate16161563.resources.customers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;

import java.util.HashMap;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com">Brian Zammit</a>
 */
@NoArgsConstructor
public class CustomerDetailsJsonBuilder {

    private String name;
    private String surname;
    private String type;
    private String status;

    public CustomerDetailsJsonBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public CustomerDetailsJsonBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public CustomerDetailsJsonBuilder setType(String type) {
        this.type = type;
        return this;
    }

    public CustomerDetailsJsonBuilder setStatus(String status) {
        this.status = status;
        return this;
    }

    public String toJsonString() throws JsonProcessingException {
        final HashMap<String, String> objectToJson = new HashMap<>();
        objectToJson.put("name", name);
        objectToJson.put("surname", surname);
        objectToJson.put("type", type);
        objectToJson.put("status", status);
        return buildJson(objectToJson);
    }

    private String buildJson(Object obj) throws JsonProcessingException {
        final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }
}
