package com.melita.interview.interviewcandidate16161563.resources.customers;

import com.melita.interview.interviewcandidate16161563.connectors.customers.InternalCustomerServiceStub;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import static org.mockito.Mockito.mock;

/**
 * @author <a href="mailto:brian.zammit.09@gmail.com">Brian Zammit</a>
 */
public class CustomerResourceTestConfiguration {

    @Bean
    @Primary
    public InternalCustomerServiceStub internalCustomerServiceStub() {
        return mock(InternalCustomerServiceStub.class);
    }
}
