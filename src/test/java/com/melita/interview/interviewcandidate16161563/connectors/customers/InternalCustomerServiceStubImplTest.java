package com.melita.interview.interviewcandidate16161563.connectors.customers;

import com.melita.interview.interviewcandidate16161563.configuration.InternalCustomerServiceProperties;
import com.melita.interview.interviewcandidate16161563.connectors.customers.requests.CreateCustomerRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import static com.melita.interview.interviewcandidate16161563.connectors.customers.requests.catalog.CustomerStatusRequest.ACTIVE;
import static com.melita.interview.interviewcandidate16161563.connectors.customers.requests.catalog.CustomerTypeRequest.REGULAR;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class InternalCustomerServiceStubImplTest {

    private static final String BASE_URL = "base-url/";

    private final RestTemplate restTemplate = mock(RestTemplate.class);
    private final InternalCustomerServiceProperties internalCustomerServiceProperties = mock(InternalCustomerServiceProperties.class);

    private InternalCustomerServiceStub internalCustomerServiceStub;

    @BeforeEach
    void setup() {
        when(internalCustomerServiceProperties.getBaseUrl()).thenReturn(BASE_URL);
        internalCustomerServiceStub = new InternalCustomerServiceStubImpl(restTemplate, internalCustomerServiceProperties);
    }

    @Test
    void create_putRequestOnEndpoint() {

        final CreateCustomerRequest request = CreateCustomerRequest.builder()
                .name("name")
                .surname("surname")
                .type(REGULAR)
                .status(ACTIVE)
                .build();
        internalCustomerServiceStub.createCustomer(request);

        verify(restTemplate).put(eq(BASE_URL + "customers"), eq(request));
    }
}