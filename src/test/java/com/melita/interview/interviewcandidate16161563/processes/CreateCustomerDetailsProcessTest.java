package com.melita.interview.interviewcandidate16161563.processes;

import com.melita.interview.interviewcandidate16161563.connectors.customers.InternalCustomerServiceStub;
import com.melita.interview.interviewcandidate16161563.connectors.customers.requests.catalog.CustomerStatusRequest;
import com.melita.interview.interviewcandidate16161563.connectors.customers.requests.catalog.CustomerTypeRequest;
import com.melita.interview.interviewcandidate16161563.connectors.customers.requests.CreateCustomerRequest;
import com.melita.interview.interviewcandidate16161563.resources.customers.models.CustomerDetails;
import com.melita.interview.interviewcandidate16161563.resources.customers.models.CustomerStatus;
import com.melita.interview.interviewcandidate16161563.resources.customers.models.CustomerType;
import com.melita.interview.interviewcandidate16161563.processes.mappers.CustomerStatusMapper;
import com.melita.interview.interviewcandidate16161563.processes.mappers.CustomerTypeMapper;
import com.melita.interview.interviewcandidate16161563.processes.sanitization.InputSanitizer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
class CreateCustomerDetailsProcessTest {

    private static final String NAME = "name";
    private static final String SURNAME = "surname";

    private static final String SANITIZED_NAME = "eman";
    private static final String SANITIZED_SURNAME = "emanrus";

    private InputSanitizer inputSanitizer = mock(InputSanitizer.class);
    private InternalCustomerServiceStub internalCustomerServiceStub = mock(InternalCustomerServiceStub.class);

    private CreateCustomerDetailsProcess createCustomerDetailsProcess = new CreateCustomerDetailsProcess(inputSanitizer,
            new CustomerTypeMapper(),
            new CustomerStatusMapper(),
            internalCustomerServiceStub);

    @BeforeEach
    void setupMocks() {
        when(inputSanitizer.sanitize(NAME)).thenReturn(SANITIZED_NAME);
        when(inputSanitizer.sanitize(SURNAME)).thenReturn(SANITIZED_SURNAME);
    }

    @Test
    void create_sanitizesNameAndSurname_callsInternalServiceStub() {
        createCustomerDetailsProcess.create(buildCustomerDetails());
        verify(internalCustomerServiceStub).createCustomer(CreateCustomerRequest.builder()
                .name(SANITIZED_NAME)
                .surname(SANITIZED_SURNAME)
                .type(CustomerTypeRequest.BUSINESS)
                .status(CustomerStatusRequest.ACTIVE)
                .build());
    }

    private CustomerDetails buildCustomerDetails() {
        final CustomerDetails customerDetails = new CustomerDetails();
        customerDetails.setName(NAME);
        customerDetails.setSurname(SURNAME);
        customerDetails.setType(CustomerType.BUSINESS);
        customerDetails.setStatus(CustomerStatus.ACTIVE);

        return customerDetails;
    }
}