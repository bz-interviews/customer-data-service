# Customer Rest API

This microservice exposes a REST API which can be used to create new customers (by performing a *POST* on the */customers* resource.

Example request body:

```yaml
{
	"name" : "CustomerName",
	"surname" : "CustomerSurname",
	"type": "REGULAR",
	"status": "ACTIVE"
}
```

Refer to included Swagger API docs for further API documentation.

This service runs on port *8443*, and requires *https* communication.

Postman collection with example request available [here](src/main/resources/Customer%20Data%20Service.postman_collection.json).


Candidate ID: *16161563*